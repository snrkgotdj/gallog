#pragma once

#include "SingletonClass.h"

#include <WinSock2.h>
#include <WS2tcpip.h>
#include <mutex>
#include <queue>

#pragma comment(lib, "ws2_32")

#include "ServerDefine.h"

class NetworkManager : public SingletonClass<NetworkManager>
{
private:
	std::queue<Packet*> packetQue_;
	std::vector<std::thread> threadPool_;
	std::mutex mutex_;

	SOCKET clientSocket_ = 0;

protected:
	virtual void beforeDestroy() override;

public:
	bool init();

public:
	void update();
	
public:
	void sendPacket(HEADER _header, const char* _data, int _size);

public:
	Packet* getPacket();
	void AddPacketQue(Packet* pPacket);

};

