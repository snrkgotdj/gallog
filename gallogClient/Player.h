#pragma once

#include "GameObject.h"

class Player : public GameObject
{

public:
	static Player* create(int playerNo);
	static Player* create(const Player& playerNo);
	void init(int _playerNo);

protected:
	virtual void update();


};

