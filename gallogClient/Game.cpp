#include "Game.h"

#include <iostream>

#include "NetworkManager.h"
#include "KeyManager.h"
#include "Player.h"

Game* Game::pInst = nullptr;

bool Game::init()
{
	NetworkManager::getInstance()->init();



	return true;
}

bool Game::progress()
{
	NetworkManager::getInstance()->update();
	KeyManager::getInstance()->update();

	clearScene();

	update();
	render();

	return true;
}

void Game::update()
{
	for (const auto& iter : mapGameObject_)
	{
		GameObject* gameObject = iter.second;
		gameObject->update();

		setPos(gameObject->vPos_, gameObject->getImage());
	}
}

void Game::render()
{
	system("cls");
	
	for (int i = 0; i < SCENE_X; ++i)
	{
		for (int j = 0; j < SCENE_Y; ++j)
		{
			printf("%c", scene_[i][j]);
		}
		printf("\n");
	}
}

void Game::clearScene()
{
	for (int i = 0; i < SCENE_X; ++i)
	{
		for (int j = 0; j < SCENE_Y; ++j)
		{
			scene_[i][j] = ' ';
		}
	}
}

void Game::addPlayer(int plyayerNo, bool isMyPlayer)
{
	Player* player = Player::create(plyayerNo);

	mapGameObject_.insert(std::make_pair(player->getObjectId(), player));

	if (isMyPlayer == true)
	{
		myPlayer_ = player;
	}
}

void Game::addPlayer(Player* _player)
{
	mapGameObject_.insert(std::make_pair(_player->getObjectId(), _player));
}

void Game::removePlayer(int playerNo)
{
	auto iter = mapGameObject_.find(playerNo);
	if (iter == mapGameObject_.end())
		return;

	mapGameObject_.erase(iter);
}

void Game::setPos(Vector2 vPos, char image)
{
	if (vPos.x >= SCENE_X || vPos.x < 0)
		return;

	if (vPos.y >= SCENE_Y || vPos.y < 0)
		return;

	scene_[vPos.x][vPos.y] = image;
}
