#pragma once
#include "SingletonClass.h"

#include <vector>
#include <list>
#include <map>
#include "Vector.h"

class Game : public SingletonClass<Game>
{
	enum { SCENE_X = 20, SCENE_Y = 20, };

private:
	std::map<int, class GameObject*> mapGameObject_;
	char scene_[SCENE_X][SCENE_Y] = {};

private:
	class Player* myPlayer_;

public:
	bool init();
	bool progress();

private:
	void update();
	void render();
	void clearScene();

public:
	void addPlayer(int plyayerNo, bool isMyPlayer);
	void addPlayer(Player* _players);

public:
	void removePlayer(int playerNo);

public:
	class Player* getMyPlayer() { return myPlayer_; };

public:
	void setPos(Vector2 vPos, char image);	

};

