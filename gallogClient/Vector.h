#pragma once

struct Vector2
{
	int x = 0;
	int y = 0;

	Vector2() {	}
	Vector2(int _x, int _y) : x(_x), y(_y) { }

};
