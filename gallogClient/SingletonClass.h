#pragma once

template <typename T>
class SingletonClass
{

protected:
	static T* pInst;
	SingletonClass() {}

protected:
	virtual void beforeDestroy() {}

public:
	static T* getInstance()
	{
		if (pInst == nullptr)
			pInst = new T;

		return pInst;
	}

	static void DestroyInstance()
	{
		if (pInst != nullptr)
		{
			pInst->beforeDestroy();
			delete pInst;
			pInst = nullptr;
		}
	}
};

template <typename T>
T* SingletonClass<T>::pInst = nullptr;