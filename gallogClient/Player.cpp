#include "Player.h"

#include "conio.h"
#include "Game.h"
#include "KeyManager.h"

Player* Player::create(int _playerNo)
{
	Player* object = new Player;
	object->init(_playerNo);

	return object;
}

Player* Player::create(const Player& _playerNo)
{
	Player* object = new Player;
	object->objectId_ = _playerNo.objectId_;
	object->vPos_ = _playerNo.vPos_;
	object->image_ = _playerNo.image_;

	return nullptr;
}

void Player::init(int _playerNo)
{
	objectId_ = _playerNo;

	vPos_.x = 10;
	vPos_.y = 15;

}

void Player::update()
{
	image_ = '&';

	if (KeyManager::getInstance()->getKeyDown(KEY::RIGHT, objectId_))
	{
		vPos_.x += 1;
	}
	else if (KeyManager::getInstance()->getKeyDown(KEY::LEFT, objectId_))
	{
		vPos_.x -= 1;
	}
	else if (KeyManager::getInstance()->getKeyDown(KEY::UP, objectId_))
	{
		vPos_.y -= 1;
	}
	else if (KeyManager::getInstance()->getKeyDown(KEY::BOTTOM, objectId_))
	{
		vPos_.y += 1;
	}
}
