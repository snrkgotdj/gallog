#include "NetworkManager.h"

#include <thread>
#include <iostream>

#include "KeyManager.h"
#include "Game.h"
#include "Player.h"

void recvThreadFunc(SOCKET socket)
{
	while (true)
	{
		Packet* pPacket = new Packet;
		int recvSize = recv(socket, (char*)pPacket, PACKET_SIZE, 0);
		// TODO : 비동기 blocking 소켓도 Packet Stream 해야할까??
		// Thread 탈출조건 해야함...
		if (recvSize == SOCKET_ERROR)
			break;

		if (recvSize == pPacket->size)
		{
			NetworkManager::getInstance()->AddPacketQue(pPacket);
		}
	}
}

bool NetworkManager::init()
{
	WSADATA wsaData;
	if (0 != WSAStartup(MAKEWORD(2, 2), &wsaData))
	{
		printf("WSAStartup 실패, 프로그램 종료");
		return 0;
	}

	clientSocket_ = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	SOCKADDR_IN clientAddr = {};
	clientAddr.sin_family = AF_INET;
	clientAddr.sin_port = htons(PORT_NUM);
	inet_pton(AF_INET, IP_ADDR, &clientAddr.sin_addr.s_addr);

	connect(clientSocket_, (SOCKADDR*)&clientAddr, sizeof(clientAddr));

	std::thread recvThread(recvThreadFunc, clientSocket_);
	threadPool_.push_back(std::move(recvThread));
	
}

void NetworkManager::beforeDestroy()
{
	for (auto& recvThread : threadPool_)
	{
		recvThread.join();
	}

	shutdown(clientSocket_, SD_BOTH);
	closesocket(clientSocket_);
	WSACleanup();
}


void NetworkManager::update()
{
	if (packetQue_.size() == 0)
		return;

	while (packetQue_.size())
	{
		Packet* pPacket = getPacket();

		switch (pPacket->header)
		{
		case HEADER::JOIN:
		{
			int clientNo = 0;
			memcpy(&clientNo, pPacket->data, sizeof(int));

			if (KeyManager::getInstance()->getPlayerNo() == -1)
			{
				Game::getInstance()->addPlayer(clientNo, true);
				KeyManager::getInstance()->setPlayerNo(clientNo);
			}
			else
			{
				Game::getInstance()->addPlayer(clientNo, false);
				Player* pPlayer = Game::getInstance()->getMyPlayer();

				sendPacket(JOIN_OTHER, (char*)pPlayer, sizeof(Player));
			}
			break;
		}
		case HEADER::JOIN_OTHER:
		{
			Player* player = new Player;
			memcpy(player, pPacket->data, sizeof(Player));
			int otherPlayerNo = player->getObjectId();

			if (otherPlayerNo == Game::getInstance()->getMyPlayer()->getObjectId())
				break;
			
			Game::getInstance()->addPlayer(player);
			break;
		}

		case HEADER::KEY_DOWN:
		{
			KEY_INFO keyInfo;
			memcpy(&keyInfo, pPacket->data, sizeof(KEY_INFO));
			KeyManager::getInstance()->setKeyDown(keyInfo);

			break;
		}

		case HEADER::CLOSE:
		{
			int clientNo = 0;
			memcpy(&clientNo, pPacket->data, sizeof(int));

			Game::getInstance()->removePlayer(clientNo);
			break;
		}

		default:
			break;
		}

		delete pPacket;
	}
}

void NetworkManager::sendPacket(HEADER _header, const char* _data, int _size)
{
	Packet packet;
	packet.header = _header;
	packet.size = sizeof(packet.header) + sizeof(packet.size) + _size;
	memcpy_s(packet.data, PACKET_SIZE, _data, _size);
	
	send(clientSocket_, (char*)&packet, packet.size, 0);
}

Packet* NetworkManager::getPacket()
{
	std::scoped_lock lock(mutex_);

	if(packetQue_.size() == 0)
		return nullptr;

	Packet* pPacket = packetQue_.front();
	packetQue_.pop();

	return pPacket;
}

void NetworkManager::AddPacketQue(Packet* pPacket)
{
	std::scoped_lock lock(mutex_);
	
	packetQue_.push(pPacket);
}


