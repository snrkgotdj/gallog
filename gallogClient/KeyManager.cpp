#include "KeyManager.h"

#include <conio.h>
#include "NetworkManager.h"

void KeyManager::setPlayerNo(int _playerNo)
{
	if (playerNo_ != -1)
		return;

	playerNo_ = _playerNo;
}

void KeyManager::setKeyDown(KEY_INFO _keyInfo)
{
	keyMap_[_keyInfo.key][_keyInfo.playerNo] = true;
}

bool KeyManager::getKeyDown(KEY _keyType, int _playerNo)
{
	auto iter = keyMap_.find(_keyType);
	if (keyMap_.end() == iter)
		return false;

	auto& playerMap = iter->second;
	auto playerMapIter = playerMap.find(_playerNo);
	if (playerMapIter == playerMap.end())
		return false;

	bool isDown = playerMapIter->second;
	playerMapIter->second = false;

	return isDown;
}

void KeyManager::update()
{
	int key = 0;
	if (_kbhit())
	{
		key = _getch();
		if (key == 224 || key == 0)
		{
			key = _getch();
			switch (key)
			{
			case (int)KEY::UP : // up
			case (int)KEY::LEFT: // left
			case (int)KEY::RIGHT: // right
			case (int)KEY::BOTTOM: // bottom
				onKeyDown((KEY)key);
			}
		}
	}

}

void KeyManager::onKeyDown(KEY _key)
{
	KEY_INFO keyInfo;
	keyInfo.key = _key;
	keyInfo.playerNo = playerNo_;

	NetworkManager::getInstance()->sendPacket(HEADER::KEY_DOWN, (const char*)&keyInfo, sizeof(KEY_INFO));
}
