#pragma once
#include "SingletonClass.h"

#include <unordered_map>

enum class KEY
{
	LEFT = 72,
	UP = 75,
	BOTTOM = 77,
	RIGHT = 80,
	END
};

struct KEY_INFO
{
	KEY key;
	int playerNo;
};

class KeyManager : public SingletonClass<KeyManager>
{
private:
	std::unordered_map<KEY, std::unordered_map<int, bool>> keyMap_;

private:
	int playerNo_ = -1;

public:
	void setPlayerNo(int _playerNo);
	void setKeyDown(KEY_INFO _keyType);
	bool getKeyDown(KEY _keyType, int _playerNo);
	int getPlayerNo() { return playerNo_; }

public:
	void update();

private:
	void onKeyDown(KEY _key);
};

