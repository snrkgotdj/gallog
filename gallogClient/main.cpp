
#include <iostream>


#include "Game.h"
#include "ServerDefine.h"



int main() 
{	
	// 게임 초기화
	if (false == Game::getInstance()->init())
	{
		printf("게임이 뭔가 잘못되서 실행할수 없다!!");
		return 0;
	}

	// 게임 실행
	while (true)
	{
		bool isProgress = Game::getInstance()->progress();

		// 종료
		if (isProgress == false)
			break;
	}

	Game::getInstance()->DestroyInstance();

	return 0;
}