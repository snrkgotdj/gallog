#pragma once

#include "Vector.h"

class GameObject
{
	friend class Game;

protected:
	Vector2 vPos_;
	int objectId_ = 0;
	char image_;

protected:
	virtual void update() {};

public:
	int getObjectId() { return objectId_;  }
	char getImage() { return image_; }

protected:
	GameObject() : vPos_(0, 0), image_{} {};

};

