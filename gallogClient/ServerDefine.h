#pragma once

#define PORT_NUM	2018
#define PACKET_SIZE 1024
#define IP_ADDR		"127.0.0.1"

enum HEADER
{
	JOIN = 0,
	JOIN_OTHER,
	KEY_DOWN,
	CLOSE,
};

class Packet
{
public:
	unsigned int size = 0;
	unsigned int header = 0;
	char data[PACKET_SIZE] = {};

public:
	Packet() {}
	Packet(unsigned int _size, unsigned int _header, const char* _data)
	{
		size = _size;
		header = _header;
		memcpy_s(data, _size, _data, _size);
	}
};