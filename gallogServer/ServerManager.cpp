#include "ServerManager.h"

ServerManager* ServerManager::pInst = nullptr;

void listenThreadFunc(SOCKET socket);
void recvThreadFunc(SOCKET socket);

void listenThreadFunc(SOCKET socket)
{
	SOCKADDR_IN clientAddr = {};
	int addrLength = sizeof(clientAddr);

	while (true)
	{
		memset(&clientAddr, 0, sizeof(SOCKADDR_IN));

		SOCKET clientSocket = accept(socket, (SOCKADDR*)&clientAddr, &addrLength);

		//int clientNo = ServerManager::getInstance()->addClientSocket(clientSocket);
		int clientNo = 0;
		printf("Client Connect %d \n", clientSocket);

		Packet packet(sizeof(int), JOIN, (char*)&clientNo);
		ServerManager::getInstance()->sendPacketAll(packet);

		std::thread recvThread(recvThreadFunc, clientSocket);
		ServerManager::getInstance()->addRecvThread(recvThread);
	}
}

void recvThreadFunc(SOCKET socket)
{
	Packet packet;

	while (true)
	{
		memset(&packet, 0, sizeof(Packet));

		// TODO : 동기 blocking 소켓도 Packet Stream을 해야할까?
		int size = recv(socket, (char*)&packet, PACKET_SIZE, 0);
		if (size == packet.size)
		{
			ServerManager::getInstance()->sendPacketAll(packet);
		}
	}
}

bool ServerManager::init()
{
	WSADATA wsaData;
	int wsaError = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (wsaError != 0)
	{
		std::cout << "WSAStartup 실패" << std::endl;
		return false;
	}

	listenSocket_ = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	WSAEVENT wsaEvent = WSACreateEvent();
	int retVal = WSAEventSelect(listenSocket_, wsaEvent, FD_ACCEPT);
	if (retVal == SOCKET_ERROR)
	{
		printf("WSAEventSelect Error");
		return false;
	}

	SOCKADDR_IN listenAddr = {};
	listenAddr.sin_family = PF_INET;
	listenAddr.sin_port = htons(PORT_NUM);
	listenAddr.sin_addr.s_addr = htonl(INADDR_ANY);

	bind(listenSocket_, (SOCKADDR*)&listenAddr, sizeof(listenAddr));
	retVal = listen(listenSocket_, SOMAXCONN);
	if (retVal == SOCKET_ERROR)
	{
		printf("listen Error");
		return false;
	}

	unsigned long on = TRUE;
	retVal = ioctlsocket(listenSocket_, FIONBIO, &on);
	if (retVal == SOCKET_ERROR)
	{
		printf("ioctlsocket Error");
		return false;
	}

	addSocket(listenSocket_, wsaEvent);

	return true;
}

void ServerManager::update()
{
	// 통신에 사용할 변수
	int index;
	WSANETWORKEVENTS networkEvents;
	SOCKET clientSocket;
	SOCKADDR_IN clientAddr;
	int nAddrLength;

	while (true)
	{
		index = WSAWaitForMultipleEvents(socketCnt_, &wsaEvents_[0], FALSE, WSA_INFINITE, FALSE);
		if (index == WSA_WAIT_FAILED)
		{
			printf("WSAWaitForMultipleEvents Error");
			continue;
		}

		index -= WSA_WAIT_EVENT_0;
		int retVal = WSAEnumNetworkEvents(allSockets_[index], wsaEvents_[index], &networkEvents);
		if (retVal == SOCKET_ERROR)
		{
			printf("WSAEnumNetworkEvents Error");
			continue;
		}

		// 모든일을 처리했따.
		if (networkEvents.lNetworkEvents == 0)
			continue;

		if (networkEvents.lNetworkEvents & FD_ACCEPT)
		{
			if (networkEvents.iErrorCode[FD_ACCEPT_BIT] != 0)
			{
				printf("errorCode : %d", networkEvents.iErrorCode[FD_ACCEPT_BIT]);
				continue;
			}

			nAddrLength = sizeof(clientAddr);
			clientSocket = accept(listenSocket_, (SOCKADDR*)&clientAddr, &nAddrLength);
			if (clientSocket == INVALID_SOCKET)
			{
				printf("accept Error");
				continue;
			}

			WSAEVENT wsaEvent = WSACreateEvent();
			retVal = WSAEventSelect(clientSocket, wsaEvent, FD_READ | FD_WRITE | FD_CLOSE);
			if (retVal == SOCKET_ERROR)
			{
				printf("WSAEventSelect Error");
				continue;
			}

			int clientNo = addClientSocket(clientSocket, wsaEvent);
			printf("Client Connect %d \n", clientSocket);

			Packet packet(sizeof(int), JOIN, (char*)&clientNo);
			ServerManager::getInstance()->sendPacketAll(packet);
		}

		if (networkEvents.lNetworkEvents & FD_READ)
		{
			if (networkEvents.iErrorCode[FD_READ_BIT] != 0)
			{
				printf("errorCode : %d", networkEvents.iErrorCode[FD_READ_BIT]);
				continue;
			}

			Packet packet;
			int retVal = recv(allSockets_[index], (char*)&packet, PACKET_SIZE, 0);
			if (retVal == SOCKET_ERROR)
			{
				printf("recv Error");
				continue;
			}
			if (retVal < 4 || retVal != packet.size)
			{
				printf("need to more recv");
				continue;
			}

			ServerManager::getInstance()->sendPacketAll(packet);
		}

		if (networkEvents.lNetworkEvents & FD_WRITE)
		{
			if (networkEvents.iErrorCode[FD_WRITE_BIT] != 0)
			{
				printf("errorCode : %d", networkEvents.iErrorCode[FD_WRITE_BIT]);
				continue;
			}
		}

		if (networkEvents.lNetworkEvents & FD_CLOSE)
		{
			int clientNo = allSockets_[index];
			
			removeSocket(index);

			Packet packet(sizeof(int), CLOSE, (char*)&clientNo);
			ServerManager::getInstance()->sendPacketAll(packet);
		}
	};
}

void ServerManager::removeSocket(int _idx)
{
	if (_idx >= socketCnt_)
		return;

	SOCKET removedSocket = allSockets_[_idx];

	shutdown(allSockets_[_idx], SD_BOTH);
	closesocket(allSockets_[_idx]);
	WSACloseEvent(wsaEvents_[_idx]);

	for (int i = _idx; i < socketCnt_ - 1; ++i)
	{
		allSockets_[i] = allSockets_[i + 1];
		wsaEvents_[i] = wsaEvents_[i + 1];
	}

	socketCnt_ -= 1;
	playerNo_ -= 1;

	printf("close Socket : %d \n", removedSocket);

}

void ServerManager::beforeDestroy()
{
	listenThread_.join();
	for (auto& recvThread : recvThreadPool_)
	{
		recvThread.join();
	}

	for (auto& clientSocket : clientSockets_)
	{
		closesocket(clientSocket);
	}

	closesocket(listenSocket_);

	WSACleanup();
}

int ServerManager::addClientSocket(SOCKET _socket, WSAEVENT _wsaEvent)
{
	std::scoped_lock lock(mutex_);

	int clientNo = playerNo_++;

	clientSockets_.push_back(_socket);
	addSocket(_socket, _wsaEvent);

	return _socket;
}

void ServerManager::addRecvThread(std::thread& thread)
{
	std::scoped_lock lock(mutex_);

	recvThreadPool_.push_back(std::move(thread));
}

void ServerManager::addSocket(SOCKET _socket, WSAEVENT _wsaEvent)
{
	if (allSockets_.size() > socketCnt_)
	{
		allSockets_[socketCnt_] = _socket;
		wsaEvents_[socketCnt_] = _wsaEvent;
	}
	else
	{
		allSockets_.push_back(_socket);
		wsaEvents_.push_back(_wsaEvent);
	}

	socketCnt_ += 1;
}

void ServerManager::sendPacketAll(const Packet& packet)
{
	std::scoped_lock lock(mutex_);

	for (const auto& client : clientSockets_)
	{
		send(client, (char*)&packet, packet.size, 0);
	}
}
