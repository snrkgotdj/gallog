#pragma once

#include <iostream>
#include <WinSock2.h>
#include <thread>
#include <vector>
#include <mutex>
#include <list>

#include "define.h"

#pragma comment(lib, "ws2_32")

class ServerManager
{
private:
	std::vector<WSAEVENT> wsaEvents_;
	std::vector<SOCKET> allSockets_;

	int socketCnt_ = 0;
	int playerNo_ = 0;

private:
	std::mutex mutex_;

private:
	SOCKET listenSocket_ = 0;
	std::vector<SOCKET> clientSockets_;

private:
	std::thread listenThread_;
	std::vector<std::thread> recvThreadPool_;

public:
	bool init();
	void update();

private:
	void removeSocket(int idx);
	void beforeDestroy();

public:
	int addClientSocket(SOCKET socket, WSAEVENT _wsaEvent);
	void addRecvThread(std::thread& thread);
	void addSocket(SOCKET _socket, WSAEVENT _wsaEvent);

public:
	void sendPacketAll(const Packet& packet);

private:
	static ServerManager* pInst;
	ServerManager() {};

public:
	static ServerManager* getInstance()
	{
		if (pInst == nullptr)
			pInst = new ServerManager;

		return pInst;
	}

	static void destroyInst()
	{
		if (pInst == nullptr)
			return;

		pInst->beforeDestroy();

		delete pInst;
		pInst = nullptr;
	}
};

